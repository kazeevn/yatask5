#!/usr/bin/python2.7
import sys
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser("logs.py: Extracts session data"
                                     " from the search logs")
    parser.add_argument('input_file', type=str,
                        help="file to process")
    parser.add_argument('output_file', type=str,
                        help="file to write result to")
    format_game = parser.add_mutually_exclusive_group(required=False)
    format_game.add_argument('-c', '--convert', action='store_true',
                        help='convert logs to the task format from'
                        ' http://switchdetect.yandex.ru/datasets')
    args = parser.parse_args()
    infile = open(args.input_file)
    outfile = open(args.output_file, 'w')
    first_line = True
    record_types = ('M', 'Q', 'C', 'S')
    for line in infile:
        # We need only session id and record type
        # maxsplit = 4, because we can encounter
        # data from the train set with an additional field
        proc_line = line.split(None, 4)
        session_id = proc_line[0]
        assert(session_id.isdigit())
        # Thankfully, day in the alternative logs can't be "M"
        record_type = proc_line[1] if proc_line[1] == 'M' else proc_line[2]
        assert(record_type in record_types)

        if not args.convert:
            # Normal output
            if record_type == 'M':
                if not first_line:
                    outfile.write("\n%s " % session_id)
                else:
                    first_line = False
                    outfile.write("%s " % session_id)
            else:
                outfile.write(record_type)
        else:
            # Converting
            if record_type == 'M':
                outfile.write("%s\t%s\t%s\n" %
                              (session_id, record_type, proc_line[3]))
            else:
                outfile.write(line)
