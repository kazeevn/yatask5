data <- read.table("countries.dat", header=TRUE, row.names=1)
km <- kmeans(data, 3, 15, nstart=10)
print(km)
pdf("countries.pdf")
plot(data, col = km$cluster)
dev.off()
clusters <- unique(km$cluster)
for (cluster in clusters) {
  print(data[km$cluster==cluster,])
}
